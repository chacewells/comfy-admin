import React from 'react';
import { Button } from 'react-bootstrap';

const AddButton = ({ onAdd }) => {
  return (
        <Button
          variant="success"
          onClick={onAdd}
          className="fa fa-plus pull-left"
        />
  );
}

export default AddButton;
