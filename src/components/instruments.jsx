import uuid from 'uuid/v4';
import RefData from './refData';
import TextCell from './table/textCell';
import instruments from '../services/instrumentsService';

class Instruments extends RefData {
  title = 'Instruments';

  state = {
    data: [],
    editing: { _id: '' },
  };

  columns = [
    {
      name: 'name',
      label: 'Name',
      onChange: e => this.handleChange(e),
      component: TextCell
    },
    this.controlsColumn()
  ];

  async componentDidMount() {
    const { data } = await instruments.list();
    this.setState({ data });
  }

  createNewItem() {
    const id = uuid();
    return {
      _id: id,
      name: ''
    };
  }
}

export default Instruments;

