import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

const NavBar = props => (
  <Navbar>
    <Nav defaultActiveKey="/">
      <NavLink className="navbar-brand" to="/">Comfy Admin</NavLink>
      <Nav.Item>
        <NavLink className="nav-link" to="/patterns">Patterns</NavLink>
      </Nav.Item>
      <Nav.Item>
        <NavLink className="nav-link" to="/colors">Colors</NavLink>
      </Nav.Item>
      <Nav.Item>
        <NavLink className="nav-link" to="/instruments">Instruments</NavLink>
      </Nav.Item>
    </Nav>
  </Navbar>
);

export default NavBar;
