import RefData from './refData';
import TextCell from './table/textCell';
import colors from '../services/colorsService';

class Colors extends RefData {
  title = 'Colors';

  state = {
    data: [],
    editing: { _id: '' },
  };

  columns = [
    {
      name: 'name',
      label: 'Name',
      onChange: e => this.handleChange(e),
      component: TextCell,
    },
    this.controlsColumn()
  ];

  async componentDidMount() {
    const { data } = await colors.list();
    this.setState({ data });
  }

  handleSave = () => {
    const { editing } = this.state;
    const data = [...this.state.data];
    const index = data.findIndex(p => p._id === editing._id);
    data[index] = editing;
    colors.createItem(editing);

    this.setState({
      editing: {_id: ''},
      data
    });
  };

  async createNewItem() {
    return await colors.createItem();
  }
}

export default Colors;
