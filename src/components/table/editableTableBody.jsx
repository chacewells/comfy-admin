import React from 'react';

/*
 * Props
 * data - the data to render
 * columns - the columns metadata
 *  - component - the type of cell to render
 *  - name - the property name
 *  - onChange - change handler for form inputs
 * editing - Item being edited
 */
class EditableTableBody extends React.Component {
  renderCell = (item, column) => {
    if (column.content) return column.content(item);

    const { component: Component, name, onChange } = column;
    const { editing } = this.props;
    return (
      <Component
        name={name}
        item={item}
        editing={editing}
        onChange={onChange} />
    );
  };

  createKey = (item, column) => {
   return item._id + (column.name || column.key);
  };

  render() {
    const { data, columns } = this.props;
    return (
      <tbody>
        {data.map(item => (
          <tr key={item._id}>
            {columns.map(column => <td key={this.createKey(item, column)}>{ this.renderCell(item, column) }</td>)}
          </tr>
        ))}
      </tbody>
    );
  }
}

export default EditableTableBody;
