import React from 'react';
import EditableCell from './editableCell';

/*
 * Props
 * name
 * item
 * onChange
 * path (optional)
 */
class CheckboxCell extends EditableCell {
  renderEdit() {
    const { editing, name, onChange } = this.props;
    return (
      <input
        type="checkbox"
        checked={editing[name]}
        name={name}
        onChange={onChange} />
    );
  }

  renderContent() {
    const { item, name } = this.props;
    return item[name] && <i className="text-success fa fa-check"></i>;
  }
}

export default CheckboxCell;
