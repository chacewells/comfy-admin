import React from 'react';
import EditableCell from './editableCell';

/*
 * Props
 * name
 * item
 * onChange
 * path (optional)
 */
class TextCell extends EditableCell {
  renderEdit() {
    const { editing, name, onChange } = this.props;
    return (
      <input
        className="form-control"
        type="text"
        value={editing[name]}
        name={name}
        onChange={onChange} />
    );
  }

  renderContent() {
    const { item, name } = this.props;
    return item[name]
  }
}

export default TextCell;
