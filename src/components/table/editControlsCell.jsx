import React, { Fragment } from 'react';
import EditableCell from './editableCell';

class EditControlsCell extends EditableCell {
  renderEdit() {
    const { onSave, onCancel } = this.props;
    return (
      <Fragment>
        <button
          className="btn text-success fa fa-check-circle"
          onClick={onSave}
        />
        <button 
          className="btn text-danger fa fa-times-circle"
          onClick={onCancel}
        />
      </Fragment>
    );
  }

  renderContent() {
    const { onToggleEdit, onDelete, item } = this.props;
    return (
      <Fragment>
        <button
          className="btn fa fa-edit"
          onClick={() => onToggleEdit(item)}></button>
        <button
          className="btn fa fa-trash"
          onClick={() => onDelete(item._id)}></button>
      </Fragment>
    );
  }
}

export default EditControlsCell;
