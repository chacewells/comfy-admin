import React, { Component, Fragment } from 'react';

class EditableCell extends Component {

  render() {
    const { editing, item } = this.props;
    return (
      <Fragment>
        {editing._id === item._id
          ? this.renderEdit()
          : this.renderContent()}
      </Fragment>
    );
  }
}

export default EditableCell;
