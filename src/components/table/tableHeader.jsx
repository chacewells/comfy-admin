import React from 'react';

const createKey = c => c.name || c.key;

const TableHeader = ({ columns }) => {
  return (
    <thead>
      <tr>
        {columns.map(c => <th key={createKey(c)}>{c.label || ''}</th>)}
      </tr>
    </thead>
  )
}

export default TableHeader;
