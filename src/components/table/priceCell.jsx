import TextCell from './textCell';
import { toUsdString } from '../../services/util';

class PriceCell extends TextCell {
  renderContent() {
    return toUsdString(super.renderContent());
  }
}

export default PriceCell;
