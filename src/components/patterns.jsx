import uuid from 'uuid/v4';
import RefData from './refData';
import TextCell from './table/textCell';
import PriceCell from './table/priceCell';
import CheckboxCell from './table/checkboxCell';
import patterns from '../services/patternsService';

class Patterns extends RefData {
  title = 'Patterns';

  state = {
    data: [],
    editing: { _id: '' },
  };

  columns = [
    {
      name: 'name',
      label: 'Name',
      onChange: e => this.handleChange(e),
      component: TextCell
    },
    {
      name: 'price',
      label: 'Price',
      onChange: e => this.handleChange(e),
      component: PriceCell
    },
    {
      name: 'image',
      label: 'Image',
      onChange: e => this.handleChange(e),
      component: TextCell
    },
    {
      name: 'can_embroider',
      label: 'Can Embroider',
      onChange: e => this.handleCheck(e),
      component: CheckboxCell
    },
    this.controlsColumn()
  ];

  async componentDidMount() {
    const { data } = await patterns.list();
    this.setState({ data });
  }

  createNewItem = () => {
    const id = uuid();
    return {
      _id: id,
      name: '',
      price: 0,
      image: '',
      can_embroider: false
    };
  };
}

export default Patterns;

