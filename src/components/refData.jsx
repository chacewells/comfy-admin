import React, { Component, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import EditableTableBody from './table/editableTableBody';
import TableHeader from './table/tableHeader';
import EditControlsCell from './table/editControlsCell';
import AddButton from './common/addButton';

class RefData extends Component {

  controlsColumn() {
    return {
      key: 'controls',
      content: (item) => (
        <EditControlsCell
          item={item}
          editing={this.state.editing}
          onSave={this.handleSave}
          onCancel={this.handleCancelRowEdit}
          onToggleEdit={this.handleToggleEdit}
          onDelete={this.handleDelete}
        />
      )
    };
  }

  handleToggleEdit = item => this.setState({ editing: {...item} });

  handleCancelRowEdit = () => this.setState({ editing: { _id: '' } });

  handleSave = () => {
    const { editing } = this.state;
    const data = [...this.state.data];
    const index = data.findIndex(p => p._id === editing._id);
    data[index] = editing;
    this.setState({
      editing: {_id: ''},
      data
    });
  };

  handleDelete = id => {
    const data = this.state.data.filter(i => i._id !== id);
    this.setState({ data });
  };

  handleChange = e => {
    const { value, name } = e.currentTarget;
    const editing = {...this.state.editing};
    editing[name] = value;
    this.setState({ editing });
  };

  handleCheck = e => {
    const { checked, name } = e.currentTarget;
    const editing = {...this.state.editing};
    editing[name] = checked;
    this.setState({ editing });
  };

  renderTable() {
    const { data, editing } = this.state;
    return (
        <Table responsive="">
          <TableHeader columns={this.columns} />
          <EditableTableBody
            data={data}
            editing={editing}
            columns={this.columns} />
        </Table>
    );
  }

  handleAdd = async () => {
    const { data: newItem } = await this.createNewItem();
    const data = [...this.state.data, newItem];
    this.setState({ editing: {...newItem}, data });
  };

  render() {
    return (
      <Fragment>
        <h1>{this.title}</h1>
        {this.renderTable()}
        <AddButton onAdd={this.handleAdd} />
      </Fragment>
    );
  }

}

export default RefData;
