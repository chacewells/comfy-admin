import RefData from './refData';

const apiEndpoint = '/colors';

const colors = new RefData(apiEndpoint);
export default colors;

