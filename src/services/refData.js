import http from './httpService';

export default class RefData {
  apiEndpoint;
  cache = [];
  constructor(apiEndpoint) {
    this.apiEndpoint = apiEndpoint;
  }

  list = () => {
    if (this.cache.length === 0) {
      return http.get(this.apiEndpoint)
        .then(resp => {
          this.cache = resp.data;
          return resp;
        });
    } else {
      return new Promise(resolve => resolve({ data: this.cache }));
    }

  };

  getItem = id => {
    return this.list().then(({ data: items }) => {
      return { data: items.find(c => c._id && c._id === id) || '' };
    });
  };

  createItem = () => {
    return http.post(`${this.apiEndpoint}/new`)
      .then(resp => {
        console.log({ resp });
        return resp;
      });
  };
}
