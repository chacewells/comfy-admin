const USD_DECIMAL = 2;

export function toUsdString(amount) {
  return `$${Number(amount).toFixed(USD_DECIMAL)}`;
}

