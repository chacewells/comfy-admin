import RefData from './refData';
const apiEndpoint = '/patterns';

const patterns = new RefData(apiEndpoint);
export default patterns;
