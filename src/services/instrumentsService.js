import RefData from './refData';
const apiEndpoint = '/instruments';

const instruments = new RefData(apiEndpoint);
export default instruments;
