import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import { Switch, Route, Redirect } from 'react-router-dom';
import NavBar from './components/navBar';
import Patterns from './components/patterns';
import Colors from './components/colors';
import Instruments from './components/instruments';
import './App.css';

function App() {
  return (
    <Container>
      <Row>
        <NavBar />
      </Row>
      <Row>
        <Switch>
          <Route path="/patterns" component={Patterns} />
          <Route path="/colors" component={Colors} />
          <Route path="/instruments" component={Instruments} />
          <Redirect from="/" to="/patterns" />
        </Switch>
      </Row>
      <br /> {/* a little gap */}
    </Container>
  );
}

export default App;
